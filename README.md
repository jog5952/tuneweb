# Tuneweb

A tool for displaying a spotify user's suggested artists through neo4j

## Prerequisites

* Neo4j - requires a running local [Neo4j](https://neo4j.com/download/) instance 
* Python2 & pip
* A public [spotify profile](https://support.spotify.com/us/article/spotify-privacy-settings/)
* Spotify [developer credentials](https://developer.spotify.com/dashboard/)
* Spotipy, a python interface for the Spotify API (pip install spotipy)

Export the Spotify ClientID, Secret, and Redirect URL:
```
export SPOTIPY_CLIENT_ID='?'
export SPOTIPY_CLIENT_SECRET='?'
export SPOTIPY_REDIRECT_URI='http://localhost'
```


### Usage

* Install the requisites above
* Run `python relatedScrape.py <username>`
* Copy the output .csv files to the import directory of your local neo4j instance
* Run the commands in the load-related-data.cypher file in neo4j

### Examples
(Red representing suggested artists and green artists currently in a user's playlists)

Call to get some highly related artists 

```
MATCH p=(a:Artist)-[:RELATED_TO]->(n:NewArtist)
WHERE n.count > 5
RETURN p
LIMIT 500
```
![Highly Related](images/highly_related.png)

See the artists with the most number of suggestions
```
MATCH p=(a:Artist)-[:RELATED_TO]->(n:NewArtist)
RETURN p
ORDER BY n.count DESC
LIMIT 500
```
![Most Related](images/most_related.png)


## Built With

* [Spotify](https://www.spotify.com) - Music streaming service used for this tool

## Authors

* **[Josh Graff](http://joshgraff.com/#)**