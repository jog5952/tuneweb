import spotipy
import operator
import csv
import unicodedata
import sys
from spotipy.oauth2 import SpotifyClientCredentials

# PreReqs:
# pip install spotipy
# export SPOTIPY_CLIENT_ID='?'
# export SPOTIPY_CLIENT_SECRET='?'
# export SPOTIPY_REDIRECT_URI='http://localhost'


def to_csv(artists, artists_to_suggestions, suggested_artists, existing_relations):
    with open('artists.csv', 'wb') as artists_csv:
        artist_writer = csv.writer(artists_csv, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for artist_uri in artists:
            artist_writer.writerow([artist_uri, artists[artist_uri]])

    with open('suggested_artists.csv', 'wb') as suggested_artists_csv:
        suggested_writer = csv.writer(suggested_artists_csv, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for artist_tuple in suggested_artists:
            suggested_writer.writerow([artist_tuple[0], artist_tuple[1]['name'], artist_tuple[1]['count']])

    with open('artists_to_suggestions.csv', 'wb') as suggestion_relations_csv:
        suggestion_relations_writer = csv.writer(suggestion_relations_csv, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for suggestion_relation_uri in artists_to_suggestions:
            for suggestion in artists_to_suggestions[suggestion_relation_uri]:
                suggestion_relations_writer.writerow([suggestion_relation_uri, suggestion])

    with open('artists_to_existing.csv', 'wb') as existing_relations_csv:
        existing_relations_writer = csv.writer(existing_relations_csv, delimiter='\t',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for suggestion_relation_uri in existing_relations:
            for suggestion in existing_relations[suggestion_relation_uri]:
                existing_relations_writer.writerow([suggestion_relation_uri, suggestion])


def find_related(sp, artists):
    suggested_artists = {}
    artists_to_suggestions = {}
    existing_relations = {}
    for artist_uri in artists:
        artists_to_suggestions[artist_uri] = []
        existing_relations[artist_uri] = []
        related = sp.artist_related_artists(artist_uri)
        for new_artist in related['artists']:
            suggested_uri = new_artist['uri']
            if not suggested_uri in artists:
                artists_to_suggestions[artist_uri].append(suggested_uri)
                if suggested_uri in suggested_artists:
                    suggested_artists[suggested_uri]['count']+=1
                else:
                    suggested_name = unicodedata.normalize('NFKD', new_artist['name']).encode('ascii','ignore')
                    suggested_artists[suggested_uri] = {'count':1, 'name': suggested_name}
            else:
                existing_relations[artist_uri].append(suggested_uri)

    return suggested_artists, artists_to_suggestions, existing_relations


def get_artists(tracks, artists):
    # Get all tracks
    for i, item in enumerate(tracks['items']):
        for artist in item['track']['artists']:
            if artist['uri']:
                artist_name = unicodedata.normalize('NFKD', artist['name']).encode('ascii', 'ignore')
                artists[artist['uri']] = artist_name
    return artists


def get_csvs():
    if len(sys.argv) != 2:
        print "Usage: relatedScrape.py <Spotify username>"
        return
    username = sys.argv[1]
    client_credentials_manager = SpotifyClientCredentials()
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
    sp.trace=False
    playlists = sp.user_playlists(username)
    artists = {}
    for playlist in playlists['items']:
        if playlist['owner']['id'] == username:
            print(playlist['name'])
            print('total tracks: '+str(playlist['tracks']['total']))
            results = sp.user_playlist(username, playlist['id'], fields="tracks,next")
            tracks = results['tracks']
            if playlist['tracks']['total'] == 0:
                continue
            artists = get_artists(tracks, artists)
            while tracks['next']:
                tracks = sp.next(tracks)
                artists = get_artists(tracks, artists)

    print("Total artists in library: "+str(len(artists)))
    suggested_artists, artists_to_suggestions, existing_relations = find_related(sp, artists)
    print("Total suggested artists: "+str(len(suggested_artists)))
    to_csv(artists, artists_to_suggestions, suggested_artists, existing_relations)

get_csvs()
