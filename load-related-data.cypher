// Run each of these commands after moving files to your neo4j import directory

// Load Artists
USING PERIODIC COMMIT
LOAD CSV FROM "file:////artists.csv" AS row FIELDTERMINATOR '\t'
CREATE (:Artist {id: row[0], name:row[1]});

// Load New Suggestions
USING PERIODIC COMMIT
LOAD CSV FROM "file:////suggested_artists.csv" AS row FIELDTERMINATOR '\t'
CREATE (:NewArtist{id: row[0], name:row[1], count:toInt(row[2])});

// Create indexes on Artist/NewArtist
CREATE INDEX ON:NewArtist(id);
CREATE INDEX ON:Artist(id);

// Create suggestion relationship
USING PERIODIC COMMIT
LOAD CSV FROM "file:////artists_to_suggestions.csv" AS row FIELDTERMINATOR '\t'
MATCH (artist:Artist),
(new:NewArtist)
WHERE row[0]=artist.id
AND row[1]=new.id
CREATE (artist)-[:RELATED_TO]->(new);
